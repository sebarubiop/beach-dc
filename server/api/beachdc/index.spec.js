'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var beachdcCtrlStub = {
  index: 'beachdcCtrl.index',
  show: 'beachdcCtrl.show',
  create: 'beachdcCtrl.create',
  upsert: 'beachdcCtrl.upsert',
  patch: 'beachdcCtrl.patch',
  destroy: 'beachdcCtrl.destroy'
};

var routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy()
};

// require the index with our stubbed out modules
var beachdcIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    }
  },
  './beachdc.controller': beachdcCtrlStub
});

describe('Beachdc API Router:', function() {
  it('should return an express router instance', function() {
    beachdcIndex.should.equal(routerStub);
  });

  describe('GET /api/beach', function() {
    it('should route to beachdc.controller.index', function() {
      routerStub.get
        .withArgs('/', 'beachdcCtrl.index')
        .should.have.been.calledOnce;
    });
  });

  describe('GET /api/beach/:id', function() {
    it('should route to beachdc.controller.show', function() {
      routerStub.get
        .withArgs('/:id', 'beachdcCtrl.show')
        .should.have.been.calledOnce;
    });
  });

  describe('POST /api/beach', function() {
    it('should route to beachdc.controller.create', function() {
      routerStub.post
        .withArgs('/', 'beachdcCtrl.create')
        .should.have.been.calledOnce;
    });
  });

  describe('PUT /api/beach/:id', function() {
    it('should route to beachdc.controller.upsert', function() {
      routerStub.put
        .withArgs('/:id', 'beachdcCtrl.upsert')
        .should.have.been.calledOnce;
    });
  });

  describe('PATCH /api/beach/:id', function() {
    it('should route to beachdc.controller.patch', function() {
      routerStub.patch
        .withArgs('/:id', 'beachdcCtrl.patch')
        .should.have.been.calledOnce;
    });
  });

  describe('DELETE /api/beach/:id', function() {
    it('should route to beachdc.controller.destroy', function() {
      routerStub.delete
        .withArgs('/:id', 'beachdcCtrl.destroy')
        .should.have.been.calledOnce;
    });
  });
});
