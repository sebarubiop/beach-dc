/**
 * Beachdc model events
 */

'use strict';

import {EventEmitter} from 'events';
import Beachdc from './beachdc.model';
var BeachdcEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BeachdcEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
for(var e in events) {
  let event = events[e];
  Beachdc.schema.post(e, emitEvent(event));
}

function emitEvent(event) {
  return function(doc) {
    BeachdcEvents.emit(event + ':' + doc._id, doc);
    BeachdcEvents.emit(event, doc);
  };
}

export default BeachdcEvents;
