'use strict';

var app = require('../..');
import request from 'supertest';

var newBeachdc;

describe('Beachdc API:', function() {
  describe('GET /api/beach', function() {
    var beachdcs;

    beforeEach(function(done) {
      request(app)
        .get('/api/beach')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          beachdcs = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      beachdcs.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/beach', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/beach')
        .send({
          name: 'New Beachdc',
          info: 'This is the brand new beachdc!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newBeachdc = res.body;
          done();
        });
    });

    it('should respond with the newly created beachdc', function() {
      newBeachdc.name.should.equal('New Beachdc');
      newBeachdc.info.should.equal('This is the brand new beachdc!!!');
    });
  });

  describe('GET /api/beach/:id', function() {
    var beachdc;

    beforeEach(function(done) {
      request(app)
        .get(`/api/beach/${newBeachdc._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          beachdc = res.body;
          done();
        });
    });

    afterEach(function() {
      beachdc = {};
    });

    it('should respond with the requested beachdc', function() {
      beachdc.name.should.equal('New Beachdc');
      beachdc.info.should.equal('This is the brand new beachdc!!!');
    });
  });

  describe('PUT /api/beach/:id', function() {
    var updatedBeachdc;

    beforeEach(function(done) {
      request(app)
        .put(`/api/beach/${newBeachdc._id}`)
        .send({
          name: 'Updated Beachdc',
          info: 'This is the updated beachdc!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedBeachdc = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedBeachdc = {};
    });

    it('should respond with the original beachdc', function() {
      updatedBeachdc.name.should.equal('New Beachdc');
      updatedBeachdc.info.should.equal('This is the brand new beachdc!!!');
    });

    it('should respond with the updated beachdc on a subsequent GET', function(done) {
      request(app)
        .get(`/api/beach/${newBeachdc._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let beachdc = res.body;

          beachdc.name.should.equal('Updated Beachdc');
          beachdc.info.should.equal('This is the updated beachdc!!!');

          done();
        });
    });
  });

  describe('PATCH /api/beach/:id', function() {
    var patchedBeachdc;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/beach/${newBeachdc._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Beachdc' },
          { op: 'replace', path: '/info', value: 'This is the patched beachdc!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedBeachdc = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedBeachdc = {};
    });

    it('should respond with the patched beachdc', function() {
      patchedBeachdc.name.should.equal('Patched Beachdc');
      patchedBeachdc.info.should.equal('This is the patched beachdc!!!');
    });
  });

  describe('DELETE /api/beach/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/beach/${newBeachdc._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when beachdc does not exist', function(done) {
      request(app)
        .delete(`/api/beach/${newBeachdc._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
