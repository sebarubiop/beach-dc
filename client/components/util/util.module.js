'use strict';

import {
  UtilService
} from './util.service';

export default angular.module('coursePlannerApp.util', [])
  .factory('Util', UtilService)
  .name;
