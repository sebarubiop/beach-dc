import angular from 'angular';
import uiRouter from 'angular-ui-router';
import routing from './main.routes';

export class MainController {
  $http;
  socket;
  beach = [];
  newThing = '';

  /*@ngInject*/
  constructor($http, $scope, socket) {
    this.$http = $http;
    this.socket = socket;

    $scope.$on('$destroy', function() {
      socket.unsyncUpdates('beach');
    });
  }

  $onInit() {
    this.$http.get('/api/beach')
      .then(response => {
        this.beach = response.data;
        this.socket.syncUpdates('beach', this.beach);
      });
  }

  addThing() {
    if (this.encnum || this.starttime || this.finishtime || this.encdate || this.medicare || this.nonmedicare) {
      this.$http.post('/api/beach', {
        encnum: this.encnum,
        starttime: this.starttime,
        finishtime: this.finishtime,
        encdate: this.encdate,
        medicare: this.medicare,
        nonmedicare: this.nonmedicare
      });
      this.encnum = '';
      this.starttime = '';
      this.finishtime = '';
      this.encdate = '';
      this.medicare = '';
      this.nonmedicare = '';
    }
  }

  deleteThing(thing) {
    this.$http.delete('/api/beach/' + thing._id);
  }
}

export default angular.module('coursePlannerApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
