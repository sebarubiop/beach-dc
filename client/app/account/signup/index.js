'use strict';

import SignupController from './signup.controller';

export default angular.module('coursePlannerApp.signup', [])
  .controller('SignupController', SignupController)
  .name;
