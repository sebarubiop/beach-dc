'use strict';

import LoginController from './login.controller';

export default angular.module('coursePlannerApp.login', [])
  .controller('LoginController', LoginController)
  .name;
