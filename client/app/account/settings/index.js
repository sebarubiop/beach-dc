'use strict';

import SettingsController from './settings.controller';

export default angular.module('coursePlannerApp.settings', [])
  .controller('SettingsController', SettingsController)
  .name;
